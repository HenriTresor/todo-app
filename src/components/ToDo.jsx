import React, { useRef, useState, useEffect } from "react";
import { Card, Button, TextField, Box, Alert, Typography, Container } from "@mui/material";
import { Delete, Edit, Add, Done, CheckBox } from "@mui/icons-material";

const ToDo = () => {
  let [taskList, setTaskList] = useState([]);
  let [isError, setIsError] = useState(false);
  let inputRef = useRef(null);
  let [editing, setEditing] = useState('')

  useEffect(() => {
    if (!localStorage.getItem('tasks-list')) {
      localStorage.setItem('tasks-list', JSON.stringify([]))
      return
    }

    setTaskList(prev => {
      return JSON.parse(localStorage.getItem('tasks-list'))

    })
  }, [])

  const addTask = () => {
    if (inputRef.current.value != "") {
      setTaskList([
        ...taskList,
        {
          id: taskList.length + 1,
          task: inputRef.current.value,
          isDone: false
        },
      ]);

      localStorage.setItem('tasks-list', JSON.stringify([
        ...taskList,
        {
          id: taskList.length + 1,
          task: inputRef.current.value,
          isDone: false
        },
      ]))
      setIsError(false);
    } else {
      setIsError(true);
    }

    inputRef.current.value = null;
    inputRef.current.focus()
  };
  const delTask = (id) => {
    let newTasks = taskList.filter((task) => task.id !== id);
    setTaskList(newTasks);
    localStorage.setItem('tasks-list', JSON.stringify(newTasks))
  };


  const makeItDone = (id) => {
    let newTasks = taskList.map((task) => {
      if (task.id === id) {
        // task.isDone = true;
        return { ...task, isDone: !task.isDone }
      }
      return task
    })

    setTaskList(newTasks);
    localStorage.setItem('tasks-list', JSON.stringify(newTasks))

  }

  const editTask = (id) => {
    let newTasks = taskList.map((task) => {
      if (task.id === id) {
        return { ...task, task: 'edited' }
      }
      return task
    })
  }
  return (
    <Container
      sx={{ mt: 5, mb: 5 }}
    >
      <Card elevation={6} sx={{ padding: 3 }}>
        <Typography
          variant="h4"
          sx={{
            marginBottom: 3,
          }}
        >
          Simple ToDo Application
        </Typography>
        <TextField
          fullWidth
          sx={{ marginBottom: 2 }}
          label="Enter The Task"
          variant="outlined"
          type="text"
          inputRef={inputRef}
          onKeyDown={(e) => { if (e.key == "Enter") { addTask() } }}
        />
        {isError ? <Alert severity="error">Please Enter a Value</Alert> : null}
        <Button variant="contained" onClick={(e) => {
          addTask()
        }} startIcon={<Add />}>
          Add
        </Button>
      </Card>

      <Box>
        <Card elevation={2}>
          {taskList != "" ? (
            taskList.map((task) => {
              return (
                <Box
                  key={task.id}
                  sx={{
                    padding: 2,
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "space-between",
                    // border: task.isDone && '2px solid ',

                    position: 'relative', mb:2
                  }}

                >
                  <Typography
                    sx={{ position: 'absolute', top: 0 ,p:0.2, }}
                  >
                    {task.isDone ? 'completed' : 'pending'}
                  </Typography>
                  <Typography component="p" variant="h4"
                    sx={{
                      textDecoration: task.isDone && 'line-through',mt:3
                    }}
                  >
                    {task.task}
                  </Typography>
                  <Box>

                    <Button>
                      <Edit>Edit</Edit>
                    </Button>
                    <Button
                      variant={task.isDone ? 'contained' : 'outlined'}
                      color='success'
                      onClick={(id) => makeItDone(task.id)}
                    >
                      <Done>Done</Done>
                    </Button>
                    <Button
                      onClick={(id) => delTask(task.id)}
                      variant="outlined"
                      color="warning"
                    >
                      <Delete>Delete</Delete>
                    </Button>
                  </Box>
                </Box>
              );
            })) : (

            <Box sx={{ textAlign: 'center', padding: 3 }}>
              <Typography component="h4" variant="h4">
                No Tasks Found
              </Typography>
            </Box>
          )}
        </Card>
      </Box>
    </Container>
  );
};

export default ToDo;
